import React, { useState } from "react";

const Quiz = () => {
const [data, setdata] = useState([
  {
    product:"Snack",
    qty:10
  },
  {
    product:"Coffee",
    qty:10
  },
  {
    product:"Bread",
    qty:10
  }
]);
const [product, setProduct] = useState("")
const [Qty, setQty] = useState(0)
return (
    <div>
      <h1>Shoping List</h1>
      <div>
      <label htmlFor="Product">Product</label>
      <br></br>
      <input type="text" onChange={(e)=>{setProduct(e.target.value)}}/> 
      </div>
      <label htmlFor="Qty">Qty</label>
      <br>
      </br>
      <input type="number" onChange={(e)=>setQty(e.target.value)}/>
      <br>
      </br>
      <br>
      </br>
      <button onClick={()=>{setdata()}}>
        Submit
      </button>
      <br>
      </br>
      <br>
      </br>
      <table border="1"> 
    <thead>
      <tr>
        <th>Product</th>
        <th>Qty</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      {data?.map((item)=>(
         <tr>
         <td>{item.product}</td>
         <td>{item.qty}</td>
         <td>
      <button onClick={()=>{setdata()}}>
        Update
      </button>
      <button onClick={()=>{setdata()}}>
        Delete
      </button>
      </td>
       </tr>
      ))}
     

    </tbody>
  </table>
    </div>
  );
};

export default Quiz;
