/**
1. Function  : Tipe data Fungsi tipe data khusus yang mewakili fungsi. Fungsi yang dimaksud adalah bol kode yang bisa dipanggil dan dijalankan di beberapa bagian program.
2. 
*/

function gabungkanDenganSpasi(Hallo, semua) {
  return Hallo + " " + semua;
}

let hasilGabung = gabungkanDenganSpasi("Hallo", "semua");
console.log(hasilGabung);
