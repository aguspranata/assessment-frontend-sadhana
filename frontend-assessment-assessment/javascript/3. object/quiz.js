/**
 * Berikan jawabanmu dibawah ini!
 */
const motor = {
  nama: "motor",
  warna: "biru",
  harga: 25000,

  start: function () {
    console.log(this.nama + " is starting...");
  },

  stop: function () {
    console.log(this.nama + " is stopping...");
  },
};

// Contoh penggunaan methods pada objek motor
motor.start(); // Output: "motor is starting..."
motor.stop(); // Output: "motor is stopping..."
