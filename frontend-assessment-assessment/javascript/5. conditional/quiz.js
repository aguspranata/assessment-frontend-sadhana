/**
 * Berikan jawabanmu dibawah ini!
 */
let angka = 10;

if (angka > 0) {
  console.log("Angka adalah bilangan positif");
}

angka = -5;

if (angka > 0) {
  console.log("Angka adalah bilangan positif");
} else {
  console.log("Angka adalah bilangan negatif atau nol");
}

angka = 0;

if (angka > 0) {
  console.log("Angka adalah bilangan positif");
} else if (angka < 0) {
  console.log("Angka adalah bilangan negatif");
} else {
  console.log("Angka adalah nol");
}
