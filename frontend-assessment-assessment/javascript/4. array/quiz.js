/**
 * Berikan jawabanmu dibawah ini!
 */
const motor = {
  nama: "motor",
  warna: "biru",
  harga: 25000,

  start: function () {
    console.log(this.nama + " is starting...");
  },

  stop: function () {
    console.log(this.nama + " is stopping...");
  },
};

const array = [motor, motor, motor, motor, motor];
console.log(array);
